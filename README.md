Запуск с нуля без лишних слов:
```sh
npm i
npx -w server knex migrate:latest
npm run build -w client
npm run start -w server
```

---

## Фронт

Запуск сервера разработки:
```sh
npm run dev -w client
```

Сборка для продакшена:
```sh
npm run build -w client
```

## Бэк

Запуск сервера:
```sh
npm run start -w server
```

Создать таблицы в базе данных:
```sh
npx -w server knex migrate:latest
```
