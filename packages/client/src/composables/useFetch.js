import { ref, watchEffect, toValue } from "vue"

import http from "../http"

export function useFetch(url, options) {
  const dataRef = ref(null)
  const errorRef = ref(null)
  const loadingRef = ref(true)

  async function fetchData() {
    loadingRef.value = true

    dataRef.value = null
    errorRef.value = null

    try {
      const { data } = await http(
        toValue(url),
        toValue(options)
      )

      dataRef.value = data
    } catch (error) {
      errorRef.value = error
    }

    loadingRef.value = false
  }

  watchEffect(() => {
    fetchData()
  })

  return {
    data: dataRef,
    error: errorRef,
    loading: loadingRef,
    refetch: fetchData
  }
}
