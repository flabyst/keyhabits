export const resizable = {
  mounted(table) {
    if ("TABLE" !== table.nodeName) {
      return console.error(
        "Директива \"v-resizable\" может быть использована только на таблице."
      )
    }

    const headers = table.querySelectorAll("th")

    let pageX

    let offsetWidth
    let headerToResize

    headers.forEach((header) => {
      header.style.position = "relative"

      const div = document.createElement("div")

      div.style.position = "absolute"
      div.style.cursor = "col-resize"
      div.style.right = "0"
      div.style.top = "0"
      div.style.height = "100%"
      div.style.width = "8px"

      div.addEventListener("mousedown", (event) => {
        headerToResize = header

        pageX = event.pageX

        offsetWidth = header.offsetWidth - event.pageX

        document.addEventListener("mousemove", handleMouseMove)
        document.addEventListener("mouseup", handleMouseUp)
      })

      function handleMouseMove(event) {
        if (! headerToResize) {
          return
        }

        headerToResize.style.width = `${offsetWidth + event.pageX}px`
      }

      function handleMouseUp(event) {
        document.removeEventListener("mousemove", handleMouseMove)
        document.removeEventListener("mouseup", handleMouseUp)
      }

      header.appendChild(div)
    })
  }
}
