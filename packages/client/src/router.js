import {
  createRouter,
  createWebHistory
} from "vue-router"

const router = createRouter({
  routes: [
    {
      path: "/",
      component: () => import("./pages/Index.vue")
    },
    {
      path: "/brands",
      children: [
        {
          path: "",
          component: () => import("./pages/Brands/Index.vue")
        },
        {
          path: "new",
          component: () => import("./pages/Brands/New.vue")
        }
      ]
    },
    {
      path: "/cars",
      children: [
        {
          path: "",
          component: () => import("./pages/Cars/Index.vue")
        },
        {
          path: "new",
          component: () => import("./pages/Cars/New.vue")
        }
      ]
    },
    {
      path: "/shops",
      children: [
        {
          path: "",
          component: () => import("./pages/Shops/Index.vue")
        },
        {
          path: ":id",
          component: () => import("./pages/Shops/Show.vue")
        },
        {
          path: "new",
          component: () => import("./pages/Shops/New.vue")
        }
      ]
    }
  ],
  history: createWebHistory()
})

export default router
