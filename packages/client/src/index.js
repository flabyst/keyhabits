import "./styles/index.scss"

import { createApp } from "vue"

import App from "./App.vue"

const app = createApp(App)

import router from "./router"

app.use(router)

import {
  resizable
} from "./directives"

app.directive("resizable", resizable)

document.addEventListener("DOMContentLoaded", () => app.mount("#app"))
