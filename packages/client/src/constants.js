const {
  VITE_APP_API_HOST
} = import.meta.env

export const API_ENDPOINT = `${location.protocol}//${VITE_APP_API_HOST}`
