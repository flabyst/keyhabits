import path from "path"

import legacy from "@vitejs/plugin-legacy"
import vue from "@vitejs/plugin-vue"

export default {
  resolve: {
    alias: {
      "~": `${__dirname}/src/`
    }
  },
  build: {
    outDir: path.resolve(
      __dirname,
      "..",
      "server",
      "public"
    )
  },
  plugins: [
    vue(),
    legacy()
  ]
}
