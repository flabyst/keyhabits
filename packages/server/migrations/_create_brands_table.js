/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.createTable("brands", (table) => {
      table.increments("id")

      table.text("name").unique()
    })
  },
  down(knex) {
    return knex.schema.dropTable("brands")
  }
}
