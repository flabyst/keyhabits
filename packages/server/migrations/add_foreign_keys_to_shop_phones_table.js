/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.alterTable("shop_phones", (table) => {
      table.foreign("phone_id").references("id").inTable("phones").onDelete("cascade")
      table.foreign("shop_id").references("id").inTable("shops").onDelete("cascade")
    })
  },
  down(knex) {
    return knex.schema.alterTable("shop_phones", (table) => {
      table.dropForeign("phone_id")
      table.dropForeign("shop_id")
    })
  }
}
