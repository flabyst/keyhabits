/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.createTable("shops", (table) => {
      table.increments("id")

      table.text("name").unique()
    })
  },
  down(knex) {
    return knex.schema.dropTable("shops")
  }
}
