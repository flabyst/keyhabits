/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.createTable("phones", (table) => {
      table.increments("id")

      table.text("phone").unique()
    })
  },
  down(knex) {
    return knex.schema.dropTable("phones")
  }
}
