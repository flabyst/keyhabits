/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.createTable("car_brands", (table) => {
      table.increments("id")

      table.integer("brand_id")
      table.integer("car_id")

      table.unique([
        "brand_id",
        "car_id"
      ])
    })
  },
  down(knex) {
    return knex.schema.dropTable("car_brands")
  }
}
