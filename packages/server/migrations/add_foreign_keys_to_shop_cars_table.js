/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.alterTable("shop_cars", (table) => {
      table.foreign("shop_id").references("id").inTable("shops").onDelete("cascade")
      table.foreign("car_id").references("id").inTable("cars").onDelete("cascade")
    })
  },
  down(knex) {
    return knex.schema.alterTable("shop_cars", (table) => {
      table.dropForeign("shop_id")
      table.dropForeign("car_id")
    })
  }
}
