/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.createTable("shop_cars", (table) => {
      table.increments("id")

      table.integer("shop_id")
      table.integer("car_id")

      table.bigInteger("price")
    })
  },
  down(knex) {
    return knex.schema.dropTable("shop_cars")
  }
}
