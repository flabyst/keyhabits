/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.alterTable("car_brands", (table) => {
      table.foreign("brand_id").references("id").inTable("brands").onDelete("cascade")
      table.foreign("car_id").references("id").inTable("cars").onDelete("cascade")
    })
  },
  down(knex) {
    return knex.schema.alterTable("car_brands", (table) => {
      table.dropForeign("brand_id")
      table.dropForeign("car_id")
    })
  }
}
