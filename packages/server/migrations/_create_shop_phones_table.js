/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.createTable("shop_phones", (table) => {
      table.increments("id")

      table.integer("phone_id")
      table.integer("shop_id")

      table.unique([
        "phone_id",
        "shop_id"
      ])
    })
  },
  down(knex) {
    return knex.schema.dropTable("shop_phones")
  }
}
