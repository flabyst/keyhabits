/** @type {import("knex").Knex.Migration} */
module.exports = {
  up(knex) {
    return knex.schema.createTable("cars", (table) => {
      table.increments("id")

      table.text("model")
    })
  },
  down(knex) {
    return knex.schema.dropTable("cars")
  }
}
