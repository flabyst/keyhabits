import dotenv from "dotenv"
import path from "path"

dotenv.config()

import express from "express"

import cors from "cors"
import helmet from "helmet"

import * as yup from "yup"

const server = express()

server.use(cors())
server.use(helmet())

server.use(express.urlencoded())
server.use(express.json())

const staticPath = path.resolve(
  __dirname,
  "..",
  "public"
)

server.use(express.static(staticPath))

import brandRoutes from "./routes/api/v1/brands"
import carRoutes from "./routes/api/v1/cars"
import shopRoutes from "./routes/api/v1/shops"
import tableRoutes from "./routes/api/v1/table"

server.use(brandRoutes)
server.use(carRoutes)
server.use(shopRoutes)
server.use(tableRoutes)

server.use((error, request, reply, next) => {
  if (error instanceof yup.ValidationError) {
    return reply.status(400).send(error.errors)
  }

  console.error(error.stack)

  reply
    .status(500)
    .end()
})

server.get(/.*/, (request, reply) => reply.sendFile(`${staticPath}/index.html`))

const {
  PORT = 8085
} = process.env

server.listen(PORT, () => console.info(`Listening on http://localhost:${PORT}...`))
