import express from "express"

import * as yup from "yup"

import database from "../../../database"

const createCarSchema = yup.object({
  brand: yup
    .string()
    .required(),
  model: yup
    .string()
    .required()
})

const router = express.Router()

router
  .get("/api/v1/cars", async (request, reply) => {
    const result = await database
      .select(
        "c.id",
        "c.model",
        "b.name as brand"
      )
      .from("cars as c")
      .join("car_brands as cb", "cb.car_id", "=", "c.id")
      .join("brands as b", "cb.brand_id", "=", "b.id")

    reply.json(result)
  })
  .post("/api/v1/cars", async (request, reply) => {
    await createCarSchema.validate(request.body, {
      abortEarly: true
    })

    const brand = request.body.brand
    const model = request.body.model

    let brandId = (
      await database
        .select("id")
        .from("brands")
        .where("name", "ilike", brand)
        .first()
    )?.id

    await database.transaction(async (trx) => {
      if (! brandId) {
        brandId = (
          await trx
            .insert({ name: brand })
            .into("brands")
            .returning("id")
        )[0].id
      }

      const carId = (
        await trx
          .insert({ model })
          .into("cars")
          .returning("id")
      )[0].id

      await trx
        .insert({
          brand_id: brandId,
          car_id: carId
        })
        .into("car_brands")
    })

    reply.end()
  })

export default router
