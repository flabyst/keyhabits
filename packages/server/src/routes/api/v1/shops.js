import express from "express"

import * as yup from "yup"

import database from "../../../database"

const createShopSchema = yup.object({
  name: yup
    .string()
    .required(),
  phone: yup
    .string()
    .required()
})

const addCarToShopSchema = yup.object({
  car: yup
    .number()
    .required(),
  price: yup
    .number()
    .required()
})

const router = express.Router()

router.param("id", async (request, reply, next, id) => {
  const shop = await database
    .select(
      "id",
      "name"
    )
    .from("shops")
    .where("id", id)
    .first()

  if (! shop) {
    return reply.status(404).end()
  }

  request.shop = shop

  next()
})

router
  .get("/api/v1/shops", async (request, reply) => {
    const result = await database
      .select(
        "id",
        "name"
      )
      .from("shops")

    reply.json(result)
  })
  .post("/api/v1/shops", async (request, reply) => {
    await createShopSchema.validate(request.body, {
      abortEarly: true
    })

    const name = request.body.name
    const phone = request.body.phone

    const shop = await database
      .select("id")
      .from("shops")
      .where("name", "ilike", name)
      .first()

    if (shop) {
      return reply.status(400).send("Магазин с таким названием уже существует.")
    }

    let phoneId = (
      await database
        .select("id")
        .from("phones")
        .where("phone", phone)
        .first()
    )?.id

    await database.transaction(async (trx) => {
      if (! phoneId) {
        phoneId = (
          await trx
            .insert({ phone })
            .into("phones")
            .returning("id")
        )[0].id
      }

      const shopId = (
        await trx
          .insert({ name })
          .into("shops")
          .returning("id")
      )[0].id

      await trx
        .insert({
          phone_id: phoneId,
          shop_id: shopId
        })
        .into("shop_phones")
    })

    reply.end()
  })

router
  .get("/api/v1/shops/:id", (request, reply) => {
    reply.send(request.shop)
  })
  .get("/api/v1/shops/:id/cars", async (request, reply) => {
    const result = await database
      .select(
        "sc.id",
        "b.name as brand",
        "c.model",
        "sc.price"
      )
      .from("shop_cars as sc")
      .where("sc.shop_id", request.shop.id)
      .join("car_brands as cb", "sc.car_id", "=", "cb.car_id")
      .join("brands as b", "cb.brand_id", "=", "b.id")
      .join("cars as c", "sc.car_id", "=", "c.id")

    reply.json(result)
  })
  .post("/api/v1/shops/:id/cars", async (request, reply) => {
    await addCarToShopSchema.validate(request.body, {
      abortEarly: true
    })

    const shop = request.shop

    const car = request.body.car
    const price = request.body.price

    await database
      .insert({
        shop_id: shop.id,
        car_id: car,
        price
      })
      .into("shop_cars")

    reply.end()
  })

export default router
