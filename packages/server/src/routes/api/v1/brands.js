import express from "express"

import * as yup from "yup"

import database from "../../../database"

const createBrandSchema = yup.object({
  name: yup
    .string()
    .required()
})

const router = express.Router()

router
  .get("/api/v1/brands", async (request, reply) => {
    const result = await database
      .select(
        "id",
        "name"
      )
      .from("brands")

    reply.json(result)
  })
  .post("/api/v1/brands", async (request, reply) => {
    await createBrandSchema.validate(request.body, {
      abortEarly: true
    })

    const name = request.body.name

    const brand = await database
      .select("*")
      .from("brands")
      .where("name", "ilike", name)
      .first()

    if (brand) {
      return reply.status(400).send("Бренд с таким названием уже существует.")
    }

    await database
      .insert({ name })
      .into("brands")

    reply.end()
  })

export default router
