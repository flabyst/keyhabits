import express from "express"

import database from "../../../database"

const router = express.Router()

router
  .get("/api/v1/table", async (request, reply) => {
    const result = await database
      .select(
        "sc.id",
        "b.name as brand",
        "c.model",
        "sc.price",
        "s.name as shop",
        "p.phone"
      )
      .from("shop_cars as sc")
      .join("car_brands as cb", "sc.car_id", "=", "cb.car_id")
      .join("brands as b", "cb.brand_id", "=", "b.id")
      .join("shops as s", "sc.shop_id", "=", "s.id")
      .join("cars as c", "sc.car_id", "=", "c.id")
      .leftJoin("shop_phones as sp", "sc.shop_id", "=", "sp.id")
      .leftJoin("phones as p", "sp.phone_id", "=", "p.id")

    reply.json(result)
  })

export default router
