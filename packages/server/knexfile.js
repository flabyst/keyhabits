require("dotenv").config()

const {
  POSTGRES_HOST,
  POSTGRES_PORT,
  POSTGRES_USERNAME,
  POSTGRES_PASSWORD,
  POSTGRES_DATABASE
} = process.env

/** @type {import("knex").Knex.Config} */
module.exports = {
  client: "pg",
  connection: {
    host: POSTGRES_HOST,
    port: POSTGRES_PORT,
    user:     POSTGRES_USERNAME,
    password: POSTGRES_PASSWORD,
    database: POSTGRES_DATABASE
  }
}
